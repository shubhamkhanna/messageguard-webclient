// ==UserScript==
// @name Polyfill script
// @description Inserts necessary polyfills into the browsers.
// @all-frames true
// @run-at document-start
// ==/UserScript==

// In Firefox, the window object does not have these properties when running page. Fixing this problem.
if (!window.encodeURI)
  window.encodeURI = encodeURI;
if (!window.encodeURIComponent)
  window.encodeURIComponent = encodeURIComponent;

// Ensure that the ShadowDOM is available.
/** ShadowDOM Polyfill is weird, and I don't currently want it included. Scott Ruoti.
if (!("createShadowRoot" in window.Element.prototype)) {
  var script = document.createElement('script');
  script.src = kango.io.getResourceUrl('res/polyfill/webcomponents.js');
  document.head.appendChild(script);
}
*/