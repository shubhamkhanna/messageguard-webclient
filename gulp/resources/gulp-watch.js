/**
 * Utility functions for watching a build task.
 */
"use strict";

var cached   = require('gulp-cached'),
    config   = require('../config'),
    gulp     = require('gulp'),
    gulpif   = require('gulp-if'),
    remember = require('gulp-remember');

/**
 *
 // Keep track of what is already being tracked so we don't watch it twice.
 * @type {Object} Keeps track of what is already being watched.
 */
var watches = {};

/**
 * Similar to gulp.src, but will also watch the files and run the given tasks if they have changed.
 * @param glob Glob for gulp.src and gulp.watch.
 * @param options Options for gulp.src and gulp.watch.
 * @param tasks The tasks to run if a change is detected.
 * @param disableCache Whether or not to disable the cache.
 * @returns {String} Name of the watch.
 */
var watch = function (glob, options, tasks, disableCache) {
  options = options || {};
  disableCache = disableCache || false;
  tasks = Array.isArray(tasks) ? tasks : [tasks];
  var name = glob.toString() + '|' + tasks.toString();

  // Add a watch.
  if (global.flags.watch && !watches.hasOwnProperty(name)) {
    watches[name] = gulp.watch(glob, options, tasks);
    if (!disableCache) {
      watches[name].on('change', function (event) {
        if (event.type === 'deleted') {
          delete cached.caches[name][event.path];
          remember.forget(name, event.path);
        }
      });
    }
  }

  return name;
};

/**
 * Similar to gulp.src, but will also watch the files and run the given tasks if they have changed.
 * @param glob Glob for gulp.src and gulp.watch.
 * @param options Options for gulp.src and gulp.watch.
 * @param tasks The tasks to run if a change is detected.
 * @param disableCache Whether or not to disable the cache.
 * @returns {} gulp.src(glob, options).
 */
var src = function (glob, options, tasks, disableCache) {
  var name = watch(glob, options, tasks, disableCache);
  return gulp.src(glob, options)
      .pipe(gulpif(global.flags.watch && !disableCache, cached(name)));
};

/**
 * Task wrapper for gulp. Creates two tasks.
 * @param name The name of the task. Tasks that you want to run from the command line should not have spaces in them.
 * @param deps An array of tasks to be executed and completed before your task will run.
 * @param fn The function that performs the task's operations. Generally this takes the form of
 *     gulp.src().pipe(someplugin()).
 */
var task = function (name, deps, fn) {
  gulp.task(name, deps, fn);
  gulp.task(name + ':nodeps', fn);
};

module.exports = {
  src:   src,
  watch: watch,
  task:  task
};