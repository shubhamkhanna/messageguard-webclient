/**
 * Create javascript bundle with browserify. Just pass the config, and we will take care of the rest!
 */
"use strict";

var assign       = require('lodash.assign'),
    browserify   = require('browserify'),
    buffer       = require('vinyl-buffer'),
    config       = require('../config'),
    gulp         = require('gulp'),
    gulpif       = require('gulp-if'),
    gutil        = require('gulp-util'),
    paramReplace = require('./param-replace'),
    source       = require('vinyl-source-stream'),
    uglify       = require('gulp-uglify'),
    watchify     = require('watchify');

/**
 * Keeps track of what is already being watched.
 * @type {Object}
 */
var watches = {};

/**
 * Build a bundle from the given files.
 * @param {Glob|Glob[]} files Files to base the bundle off of.
 * @param {string} name Name of the bundle file.
 * @param {Object} [options] Options for customizing the bundling.
 * @param {string} [buildPath] Optional build path, otherwise will use the one from config.js.buildPath.
 */
function bundle(files, name, options, buildPath) {
  // If this bundle is already being watched, we don't need to do anything for it.
  if (watches.hasOwnProperty(name)) {
    console.error("Recreating an already created bundle.");
    return gulp.src([]);
  }
  watches[name] = true;

  options = options || {};

  var browserifyConfig = {
    entries: files,
    debug:   global.flags.debug
  };

  if (options.browserify) {
    assign(browserifyConfig, options.browserify);
  }
  if (global.flags.watch) {
    assign(browserifyConfig, watchify.args);
  }

  var b = browserify(browserifyConfig);

  var buildBundle = function () {
    return b.bundle()
        .on('error', gutil.log)
        .pipe(source(name))
        .pipe(buffer())
        .pipe(paramReplace())
        .pipe(gulpif(!global.flags.debug, uglify()))
        .pipe(gulp.dest(buildPath || config.js.buildPath));
  };

  if (global.flags.watch) {
    b = watchify(b);
    b.on('update', buildBundle);
    b.on('log', gutil.log);
  }

  return buildBundle();
}
module.exports = bundle;