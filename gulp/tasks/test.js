/**
 * Test tasks.
 */
var brfs         = require('brfs'),
    bundle       = require('../resources/bundle'),
    config       = require('../config'),
    del          = require('del'),
    gulp         = require('gulp'),
    gwatch       = require('../resources/gulp-watch'),
    merge        = require('merge-stream'),
    paramReplace = require('../resources/param-replace'),
    rename       = require('gulp-rename'),
    sftp         = require('gulp-sftp');

// Upload files to the server.
gwatch.task('test:upload', function () {
  return gwatch.src(config.test.buildFiles, null, ['test:upload'])
      .pipe(sftp(config.test.sftpOptions));
});

// Clean up any temporary files.
gulp.task('test:clean', ['js:clean', 'sass:clean'], function () {
  return del(config.test.buildPath);
});

// Get the general files.
gwatch.task('test:files', ['js', 'sass'], function () {
  return merge(
      // Handle the javascript.
      gwatch.src(config.js.buildFiles, null, ['test:files:nodeps'])
          .pipe(gulp.dest(config.test.buildPath + 'js')),

      // Add HTML files to the root directory.
      gwatch.src(config.html.srcFiles, null, ['test:files:nodeps'])
          .pipe(gulp.dest(config.test.buildPath + 'html')),

      // Include the css files.
      gwatch.src(config.sass.buildFiles, null, ['test:files:nodeps'])
          .pipe(gulp.dest(config.test.buildPath)),

      // Include remaining resources.
      gwatch.src(config.resources.srcFiles, null, ['test:files:nodeps'])
          .pipe(gulp.dest(config.test.buildPath))
  );
});

// Create the performance test.
gwatch.task('test:performance', ['test:files', 'test:performance:js'], function () {
  return merge(
      gwatch.src(config.test.performanceFiles, null, ['test:performance:nodeps'])
          .pipe(gulp.dest(config.test.buildPath)),

      gwatch.src(config.test.buildPath + 'test-bundle.js', null, 'test:performance:nodeps')
          .pipe(rename('test.js'))
          .pipe(paramReplace())
          .pipe(gulp.dest(config.test.buildPath))
  );
});

// Bundle the performance test.
gulp.task('test:performance:js', function () {
  return bundle(config.test.performancePath + 'test.js', 'test-bundle.js', {browserify: {transform: brfs}},
                config.test.buildPath);
});