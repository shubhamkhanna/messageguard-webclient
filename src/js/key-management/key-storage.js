/**
 * Stores key systems
 */

var encoder       = require('../common/encoder'),
    Encryptor     = require('../common/encryptor'),
    Errors        = require('../common/errors'),
    EventNames    = require('./communicator/event-names'),
    EventEmitter  = require('../common/storage/storage-objects').eventEmitter,
    KeySchemes    = require('./key-schemes'),
    KeyAttributes = require('./key-prototypes').KeyAttributes,
    Storage       = require('../common/storage');

/**
 * Prevents master password from being prompted.
 */
var HARDCODED_MASTER_PASSWORD = null;
HARDCODED_MASTER_PASSWORD = Encryptor.passwordToKey('messageguard-pass', '');

/**
 * Retrieve the master password, throw an error if not present.
 *
 * @param suppressError - if true, just return null instead of throwing an error.
 */
function getSessionMasterPassword(suppressError) {
  return Storage.managedSession.getItem('master-password').then(function (password) {
    password = HARDCODED_MASTER_PASSWORD || password;

    if (password || suppressError) {
      return password;
    } else {
      throw new Errors.MasterPasswordNotFoundError();
    }
  });
}

/**
 * Retrieve the session master password hash, to verify a given password.
 */
function getSessionMasterPasswordHash() {
  return Storage.local.getItem('master-password-hash');
}

/**
 * Encrypt a key system for storage. If the key's scheme does not use encrypted storage,
 * just return the serialized key.
 *
 * @param keySystem - key system to serialize and encrypt.
 */
function encryptKeySystem(keySystem) {
  return KeySchemes.getScheme(keySystem.attributes.scheme).serialize(keySystem).then(function (serializedKey) {
    var toEncrypt = {
      encrypted:  false,
      serialized: serializedKey,
      attributes: keySystem.attributes.serialize()
    };

    if (!KeySchemes.getScheme(keySystem.attributes.scheme).usesEncryptedStorage) {
      return toEncrypt;
    }

    return getSessionMasterPassword().then(function (password) {
      var toEncryptStr = JSON.stringify(toEncrypt);

      return {
        encrypted:     true,
        encryptedData: Encryptor.encrypt(toEncryptStr, password)
      };
    });
  });
}

/**
 * Decrypt a serialized system. The system might not have been encrypted, if the scheme doesn't rely on encrypted
 * storage. In that case just parse the system and return without decrypting first.
 *
 * @param encryptedData - data to be decrypted.
 * @param password - if given, attempt to decrypt the key with this password. If not, use the master password.
 * @returns {*|Promise} - decrypted key system.
 */
function decryptKeySystem(encryptedData, password) {

  /**
   * Given an object of decrypted data, parse out the key system and return it.
   * @param decryptedData - object of stringified key system data.
   */
  var parseDecryptedData = function (decryptedData) {
    var attributes = KeyAttributes.parse(decryptedData.attributes);

    return KeySchemes.getScheme(attributes.scheme).parse(decryptedData.serialized).then(function (system) {
      system.attributes = attributes;
      return system;
    });
  };

  if (!encryptedData.encrypted && encryptedData.serialized && encryptedData.attributes) {
    return parseDecryptedData(encryptedData);
  }

  // If the password is given, resolves to the given password; else, resolves to the session master password.
  var passPromise = password ? Promise.resolve(password) : getSessionMasterPassword();

  return passPromise.then(function (password) {
    var decrypted = Encryptor.decrypt(encryptedData.encryptedData, password);
    decrypted = JSON.parse(decrypted);
    return parseDecryptedData(decrypted);
  }).catch(function (e) {
    if (e instanceof Errors.EncryptionError) {
      throw new Errors.KeyStorageError('Could not decrypt storage.');
    } else {
      throw e;
    }
  });
}

/**
 * Given a string label for a storage type, return the actual storage object.
 *
 * @param storageType - name of storage object to retrieve.
 */
function getStorage(storageType) {
  switch (storageType) {
    case 'local':
      return Storage.local;
    case 'session':
      return Storage.managedSession;
    default:
      throw new Errors.KeyStorageError('Illegal storage type.');
  }
}

/**
 * Given a fingerprint, transform it into a storage key,
 * so we can identify it as a storage key we care about in the future.
 *
 * @param fingerprint - fingerprint of key we want to store or look up.
 * @returns {string}  - storage key prefixed with 'key-system:'
 */
function getStorageKey(fingerprint) {
  return 'key-system:' + fingerprint;
}

/**
 * Tests whether a given storage key is one we care about, that has the 'key-system:' prefix.
 * @param key         - string object key
 * @returns {boolean} - whether the given storage key is one we care about.
 */
function isStorageKey(key) {
  return /^key-system:/.test(key);
}

/**
 * Filters all storage keys for the given type, and returns those that are ones we care about.
 * @param storageType - string representing what type of storage to look at.
 */
function getStorageKeys(storageType) {
  return getStorage(storageType).getKeys().then(function (keys) {
    return keys.filter(isStorageKey);
  });
}

/**
 * Function to pass to Array.prototype.sort() to alphabetize systems.
 */
function _alphabetizeSystems(a, b) {
  return _alphabetizeAttributes(a.attributes, b.attributes);
};

/**
 * Function to pass to Array.prototype.sort() to alphabetize system attributes.
 */
function _alphabetizeAttributes(a, b) {
  return a.name > b.name;
}

module.exports = {
  /**
   * Adds a key system to storage.
   *
   * @param keySystem
   */
  add: function (keySystem) {
    var self = this;
    
    // First, ensure we'll have a unique name, by adding a number to the end if necessary.
    return self.getAllAttributes().then(function (allAttributes) {
      var allNames = allAttributes.map(function (attributes) {
        return attributes.name;
      });

      if (allNames.indexOf(keySystem.attributes.name) !== -1) {
        var oldKeyName = keySystem.attributes.name;
        var i = 2;

        while (allNames.indexOf(oldKeyName + ' (' + i + ')') !== -1) {
          i++;
        }
        
        keySystem.attributes.name = oldKeyName + ' (' + i + ')'
      }
    }).then(function () {
      return encryptKeySystem(keySystem).then(function (encryptedData) {

        var storageKey = getStorageKey(keySystem.attributes.fingerprint);

        return getStorage(keySystem.attributes.storage).setItem(storageKey, {
          scheme:        keySystem.attributes.scheme,
          attributes:    keySystem.attributes.serialize(),
          encryptedData: encryptedData,
        });
      }).then(function () {
        return self._onKeysUpdated();
      });
    });
  },

  /**
   * Returns a KeySystem identified by the given fingerprint
   *
   * @param fingerprint
   * @param password - if given, attempt to decrypt using that password
   * @returns {Object}
   */
  get: function (fingerprint, password) {
    var storageKey = getStorageKey(fingerprint);

    return Promise.all([
                         getStorage('local').getItem(storageKey),
                         getStorage('session').getItem(storageKey)
                       ]).then(function (results) {
      var keyData = results[0] || results[1];

      if (!keyData) {
        throw new Errors.KeyNotFoundError('Key not found for fingerprint: ' + fingerprint);
      }

      return decryptKeySystem(keyData.encryptedData, password);
    });
  },

  /**
   * Returns all the key systems for the given key scheme.
   *
   * @param schemeName
   * @returns {Array.<Object>}
   */
  getByScheme: function (schemeName) {
    return this.getAll().then(function (keysArray) {
      return keysArray.filter(function (key) {
        return key.attributes.scheme === schemeName;
      });
    });
  },

  /**
   * Returns all the key system attributes for the given key scheme.
   *
   * @param schemeName
   * @returns {Array.<Object>}
   */
  getAttributesByScheme: function (schemeName) {
    return this.getAllAttributes().then(function (attributesArray) {
      return attributesArray.filter(function (attributes) {
        return attributes.scheme === schemeName;
      })
    });
  },

  /**
   * Returns all the key systems available
   *
   * @returns {Array.<Object>}
   */
  getAll: function () {
    var getKeys = function (storageType) {
      return getStorageKeys(storageType).then(function (storageKeys) {
        return Promise.all(storageKeys.map(function (storageKey) {
          return getStorage(storageType).getItem(storageKey);
        })).then(function (items) {
          return Promise.all(items.filter(function (item) {
            if (item && item.encryptedData && KeySchemes.getNames().indexOf(item.scheme) != -1) {
              return true
            } else {
              return false;
            }
          }).map(function (item) {
            return decryptKeySystem(item.encryptedData);
          }));
        });
      });
    };

    return Promise.all([
                         getKeys('local'),
                         getKeys('session')
                       ]).then(function (results) {

      var allKeys = [].concat(results[0], results[1]);
      return allKeys.sort(_alphabetizeSystems);
    });
  },

  /**
   * Removes all keys from storage.
   */
  clearAll: function () {
    return Promise.all(['local', 'session'].map(function (storageType) {
      return getStorageKeys(storageType).then(function (storageKeys) {
        return Promise.all(storageKeys.map(function (key) {
          return getStorage(storageType).removeItem(key);
        }));
      });
    }));
  },

  /**
   * Returns all the key system attributes available
   *
   * @returns {Array.<Object>}
   */
  getAllAttributes: function (rawArray) {
    /**
     * For a given storage type, return all key attributes in that storage area.
     *
     * @param storageType - either 'local' or 'session'.
     */
    var getKeyAttributes = function (storageType) {
      return getStorageKeys(storageType).then(function (storageKeys) {
        return Promise.all(storageKeys.map(function (storageKey) {
          return getStorage(storageType).getItem(storageKey);
        })).then(function (items) {
          return items.filter(function (item) {
            if (item && item.attributes && KeySchemes.getNames().indexOf(item.scheme) != -1) {
              return true;
            } else {
              return false;
            }
          }).map(function (item) {
            return KeyAttributes.parse(item.attributes);
          });
        });
      });
    };

    return Promise.all(['local', 'session'].map(getKeyAttributes)).then(function (results) {
      return [].concat(results[0], results[1]).sort(_alphabetizeAttributes);
    });
  },

  /**
   * Updates a KeySystem
   *
   * @param keySystem
   */
  update: function (keySystem) {
    var self = this;

    return this.add(keySystem).then(function () {
      return self._onKeysUpdated();
    });
  },

  /**
   * Deletes a KeySystem with the associated fingerprint
   * NOTE: named 'delete' in MG paper, using "remove" instead to avoid
   * use of keyword as function name.
   *
   * @param keySystem
   */
  remove: function (keySystem) {
    var storageKey = getStorageKey(keySystem.attributes.fingerprint);
    return getStorage(keySystem.attributes.storage)
        .removeItem(storageKey)
        .then(this._onKeysUpdated.bind(this));
  },

  /**
   * Returns true if there is a scheme that uses encrypted storage,
   * and we don't have a master password hardcoded.
   */
  needsMasterPassword: function () {
    // If there's a scheme that needs a master password, and we don't have one hardcoded,
    // return true.
    if (KeySchemes.hasSchemeWithEncryptedStorage() && !HARDCODED_MASTER_PASSWORD) {
      return true;
    } else {
      return false;
    }
  },

  /**
   * Checks to make sure that we have a valid master password in session storage.
   * Throws an error if we do not.
   */
  ensureMasterPasswordPresent: function () {

    // Don't bother checking for the master password if there isn't a scheme that
    // uses encrypted storage anyways.
    if (!KeySchemes.hasSchemeWithEncryptedStorage() || HARDCODED_MASTER_PASSWORD) {
      return Promise.resolve();
    }

    var self = this;

    return getSessionMasterPassword(true).then(function (password) {
      if (!password) {
        return getSessionMasterPasswordHash().then(function (passwordHash) {
          var errorType;

          if (passwordHash) {
            errorType = Errors.MasterPasswordNeedsReentering;
          } else {
            errorType = Errors.MasterPasswordMissing;
          }

          throw new errorType();
        });
      }

      return self.checkMasterPassword(password, true).then(function (result) {
        if (!result) {
          throw new Errors.MasterPasswordNotFoundError();
        }
      });
    });
  },

  /**
   * Sets the master password to the given value. Calculates a password has to store in persistent local
   * storage as well.
   *
   * @param password - password to store.
   */
  setMasterPassword: function (password) {
    // If we already have a master password hash, use it to generate a PBKDF2 encryption key from the given password.
    // If not, generate a new hash based on the password.

    return getSessionMasterPasswordHash().then(function (passwordHash) {
      var next;
      if (passwordHash) {
        next = Promise.resolve();
      } else {
        passwordHash = Encryptor.passwordToHash(password);
        next = Storage.local.setItem('master-password-hash', passwordHash);
      }

      next.then(function () {
        var key = Encryptor.passwordKeyFromPasswordAndHash(password, passwordHash);
        return Storage.managedSession.setItem('master-password', key);
      });
    });
  },

  /**
   * Checks to see whether the given password is valid. Makes sure it can decrypt all stored encryption keys.
   *
   * @param password - given password to check
   * @param skipDerivation - if true, the password has already been turned into an encryption key.
   *                         This is true if it's called from ensureMasterPasswordPresent.
   */
  checkMasterPassword: function (password, skipDerivation) {
    var self = this;

    return getSessionMasterPasswordHash().then(function (passwordHash) {
      // If we're checking an actual password, and the hash exists, and it doesn't match, return false.
      if (!skipDerivation && passwordHash && !Encryptor.verifyPasswordHash(password, passwordHash)) {
        return false;
      }

      return self.getAllAttributes().then(function (attributesArray) {
        // If there are no keys in storage, we're done checking.
        // If there are keys in storage, but we have no password hash to use to re-derive the
        // encryption key (using the hash's salt), then we're hosed and we just bail out.

        if (attributesArray.length == 0) {
          return true;
        } else if (!passwordHash) {
          return false;
        }

        // There are passwords in storage; perform key derivation and attempt to decrypt them.
        if (!skipDerivation) {
          password = Encryptor.passwordKeyFromPasswordAndHash(password, passwordHash);
        }

        // If any decryptions fail, return false.
        return Promise.all(attributesArray.map(function (attributes) {
          return self.get(attributes.fingerprint, password);
        })).then(function () {
          return true;
        }).catch(function () {
          return false;
        });
      });
    });
  },

  /**
   * Retrieves the draft encryption key from encrypted storage.
   * If it is not present, a new one is generated.
   */
  getDraftEncryptionKey: function () {
    return getSessionMasterPassword().then(function (password) {
      return Storage.local.getItem('encrypted-draft-key').then(function (key) {
        if (key) {
          return key;
        } else {
          key = Encryptor.encrypt(Encryptor.randomKey(), password);

          return Storage.local.setItem('encrypted-draft-key', key).then(function () {
            return key;
          });
        }
      }).then(function (encryptedKey) {
        return Encryptor.decrypt(encryptedKey, password);
      });
    });
  },

  /**
   * Called whenever keys are updated. Lets the overlays know to update their key attribute listings.
   * @private
   */
  _onKeysUpdated: function () {
    return this.getAllAttributes().then(function (attributesArray) {
      EventEmitter.emit(EventNames.KEYS_UPDATED, {
        attributes: attributesArray.map(function (attributes) {
          return attributes.serialize();
        })
      });
    });
  }
};
