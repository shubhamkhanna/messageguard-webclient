/*
* Shared symmetric key that encrypts via AES
*/

var IBE  = require('ibejs'),
    util = require('util');

var $             = require('../../common/jquery-bootstrap'),
    Ajax          = require('../../common/ajax'),
    Errors        = require('../../common/errors'),
    encoder       = require('../../common/encoder'),
    Encryptor     = require('../../common/encryptor'),
    KeyPrototypes = require('../key-prototypes');

Errors.registerErrorType(Errors.KeyManagerError, 'IBEError');

// ******************************
//
// Key Scheme
//
// ******************************


var SCHEME_NAME = 'IBE';

var KEYSERVER_HOSTNAME = 'keys.messageguard.io';

var IBEScheme = function () {
  this.name = SCHEME_NAME;
};

IBEScheme.prototype = new KeyPrototypes.KeySchemeBase();

/**
 * Called if there are no IBE systems currently in key storage.
 *
 * Gets a list of identities currently known to the key server, and
 * automatically creates keys for each identity.
 *
 * @promise {IBESystem[]} - list of new IBE systems created
 * @reject error - AJAX error if encountered
 */
IBEScheme.prototype.init = function (callbacks) {
  var self = this;

  var errorMessage = 'Error initializing IBE keys. Please make sure you are signed in to an account at <a href="https://' +
                     KEYSERVER_HOSTNAME + '" target="_blank">MessageGuard.io</a> and have validated an identity.';

  return callbacks.getAllSchemeAttributes().then(function (existingAttributes) {
    if (existingAttributes.length) {
      // Don't bother fetching an ID list from the keyserver if we already have keys.
      return;
    }

    return new Promise(function (resolve, reject) {
      Ajax.post(
          {
            url:             'https://' + KEYSERVER_HOSTNAME + '/api/whoami',
            withCredentials: true,
            responseType:    'json',

            success:         function (data) {
              if (!data.identities) {
                return reject(new Errors.KeyInitializationError(errorMessage));
              }

              Promise.all(data.identities.map(function (identity) {
                identity = new KeyPrototypes.Identifier(identity.value, identity.type);

                return self._verifyID(identity).then(function (keys) {
                  var system = new IBESystem(keys.publicParams, keys.privateKey);
                  system._setID(identity);

                  system.attributes.name = util.format('%s key (%s)', SCHEME_NAME, identity.value);

                  return callbacks.addKey(system);
                });
              })).then(resolve);
            },

            error:           function (statusCode, response) {
              reject(new Errors.KeyInitializationError(errorMessage));
            }
          });
    });
  });
};

/**
 * Given an IBESystem and callback for setting save-state, produce
 * an element configured for editing the key system.
 *
 * @param {IBESystem} keySystem - system for editing. Might be null, in which case this is a new system editor.
 * @callback callbacks - object with callbacks to interact with UI manager.
 * @promise {JQuery} - element for editing the key system.
 */
IBEScheme.prototype.getUI = function (keySystem, callbacks) {
  var self = this;

  var editor = $('<div> \
  					<div class="input-group"> \
  					  <select class="ibe-id-type form-control" style="width:20%"> \
						  <option value="Email" selected="selected">Email</option> \
						  <option value="Facebook">Facebook</option> \
						  <option value="MessageGuard" disabled>MessageGuard</option> \
						</select> \
					  <input type="text" class="ibe-id form-control" placeholder="Enter your email address" style="width:80%"> \
					  <span class="input-group-btn"> \
						<button class="verify-btn btn btn-default" type="button" data-default-text="Verify" data-success-text="Verified" data-error-text="Error"></button> \
					  </span> \
					</div> \
  				<div class="alert alert-danger" role="alert"> \
  					<div class="row"> \
  						<div class="col-md-10">\
					  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> \
					  <span class="alert-text"></span> \
					</div> \
					<br /><br />\
					<div class="col-md-2"> \
					<button type="button" class="retry-btn btn btn-info">Retry</button> \
					</div> \
					</div> \
					</div> \
				</div>');

  var idTypeSelect = editor.find('.ibe-id-type');
  var idField = editor.find('.ibe-id');
  var verifyBtn = editor.find('.verify-btn');
  var alertDiv = editor.find('.alert');
  var retryBtn = alertDiv.find('.retry-btn');

  /**
   * Configures the verify-btn element's text value.
   *
   * @param {string} status - status name to set text for.
   */
  var setVerifyBtnText = function (status) {
    var attr_name = 'data-' + status + '-text';

    verifyBtn.text(verifyBtn.attr(attr_name));
  };

  /**
   * Shows an alert on error.
   *
   * @param {string} alert - HTML representation of an alert value.
   * @callback tryAgainCB - function to be called when the user wants to try the action again.
   */
  var showAlert = function (alert, tryAgainCB) {
    if (alert) {
      alertDiv.find('.alert-text').html(alert);
      retryBtn.prop('disabled', false).toggle(typeof tryAgainCB != 'undefined')
          .off('click').click(function () {
        $(this).prop('disabled', true);
        tryAgainCB();
      });

      alertDiv.show();
    } else {
      alertDiv.hide();
    }
  };

  idTypeSelect.val(keySystem ? keySystem.attributes.id.type : idTypeSelect.find('option[selected]').val());
  idField.val(keySystem ? keySystem.attributes.id.value : '');
  setVerifyBtnText(keySystem ? 'success' : 'default');
  verifyBtn.prop('disabled', true);
  callbacks.isSaveable(keySystem ? true : false);
  showAlert(null);

  var verifiedID = new KeyPrototypes.Identifier();

  /**
   * Checks whether the ID has remained unchanged from the last-verified ID.
   *
   * @returns {boolean}
   */
  var idUnchanged = function () {
    return idTypeSelect.val() === verifiedID.type && idField.val() === verifiedID.value;
  };

  idTypeSelect.add(idField).on('input change propertychange paste', function () {
    setVerifyBtnText('default');
    callbacks.isSaveable(idUnchanged());
    verifyBtn.prop('disabled', idUnchanged());
  });

  /**
   * Function to verify ownership of the entered ID.
   * If successful, enables saving.
   * If unsuccessful, displays an appropriate error.
   */
  var verifyID = function () {
    verifyBtn.prop('disabled', true);

    var id = new KeyPrototypes.Identifier(idField.val(), idTypeSelect.val());

    self._verifyID(id).then(function () {
      callbacks.isSaveable(true);
      showAlert(null);
      setVerifyBtnText('success');

      verifiedID.type = id.type;
      verifiedID.value = id.value;
    }).catch(function (err) {
      callbacks.isSaveable(false);
      setVerifyBtnText('default');

      var errText = {
        _401:     'Your identity needs to be verified. Visit <a target="_blank" href="https://' + KEYSERVER_HOSTNAME +
                  '[LOGIN_URL]">' + KEYSERVER_HOSTNAME + '</a> to log in and verify your identity.',
        _404:     'Your identity needs to be registered. Visit <a target="_blank" href="https://' + KEYSERVER_HOSTNAME +
                  '">' + KEYSERVER_HOSTNAME + '</a> to register an account and verify your identity.',
        _default: 'Unknown error occurred. Visit <a target="_blank" href="https://' + KEYSERVER_HOSTNAME + '">' +
                  KEYSERVER_HOSTNAME + '</a> to resolve.'
      };

      switch (err.details.code) {
        case 404:
          showAlert(errText._404, verifyID);
          break;

        case 401:
          showAlert(errText._401.replace('[LOGIN_URL]', err.details.text), verifyID);
          break;

        default:
          showAlert(errText._default, verifyID);
      }
    });
  };

  verifyBtn.click(verifyID);

  return Promise.resolve(editor);
};

/**
 * Verifies a given identity against the key server to ensure the
 * key server will grant access to the identity's private key.
 *
 * If verification is successful, the private key and public parameters will be returned.
 *
 * @param {string} id
 * @promise {Object.<string, number[]>} - object containing the private key and public parameters.
 * @reject {Object.<string, string>} - error object
 * @private
 */
IBEScheme.prototype._verifyID = function (id) {
  var idString = JSON.stringify({
                                  identity: {
                                    type:  id.type,
                                    value: id.value
                                  }
                                });

  return new Promise(function (resolve, reject) {
    Ajax.post({
                url:             'https://' + KEYSERVER_HOSTNAME + '/api/private/ibe/',
                withCredentials: true,
                contentType:     'application/json',
                responseType:    'json',
                data:            idString,
                success:         function (response) {
                  resolve(response);
                },
                error:           function (code, response) {
                  reject(new Errors.IBEError('Failed to verify ownership of identity: ' + id.serialize(), response));
                }
              });
  });
};

IBEScheme.prototype.handleError = function (keyError, editor, resolved) {
  // TODO
};

/**
 * Given an element for an editor, create a new IBESystem instance.
 *
 * @param {JQuery} editor
 */
IBEScheme.prototype.create = function (editor, fingerprintsToMatch) {
  var id_value = $(editor).find('.ibe-id').val();
  var id_type = $(editor).find('.ibe-id-type').val();

  var id = new KeyPrototypes.Identifier(id_value, id_type);

  return this._verifyID(id).then(function (keys) {
    var system = new IBESystem(keys.publicParams, keys.privateKey);
    system._setID(id);

    if (fingerprintsToMatch && fingerprintsToMatch.length) {
      var matched = false;
      for (var i = 0; i < fingerprintsToMatch.length; i++) {
        matched |= system.attributes.fingerprint == fingerprintsToMatch[i];
      }

      if (!matched) {
        throw new Errors.KeyCreateFailedError('Key does not match any fingerprints provided.');
      }
    }

    return system;
  });
};

/**
 * Given an element for an editor and an existing IBESystem instance, update the instance.
 *
 * @param {IBESystem} keySystem - system to update.
 * @param {JQuery} editor - element representing the new key.
 */
IBEScheme.prototype.update = function (keySystem, editor) {
  var id_value = $(editor).find('.ibe-id').val();
  var id_type = $(editor).find('.ibe-id-type').val();

  var id = new KeyPrototypes.Identifier(id_value, id_type);

  return this._verifyID(id).then(function (keys) {
    keySystem._setKeyMaterial(keys.publicParams, keys.privateKey);
    keySystem._setID(id);
  });
};

/**
 * Serializes a system into a string representation.
 *
 * @param {IBESystem} keySystem - system to serialize.
 * @promise {string} - serialized representation.
 */
IBEScheme.prototype.serialize = function (keySystem) {
  var serialized = JSON.stringify({
                                    publicParams: keySystem.publicParams,
                                    privateKey:   keySystem.privateKey
                                  });

  return Promise.resolve(serialized);
};

/**
 * Parses an IBESystem instance from a serialized representation.
 *
 * @param {string} serializedSystem - serialized representation of a key system.
 * @promise {IBESystem} - parsed system
 */
IBEScheme.prototype.parse = function (serializedSystem) {
  var d = JSON.parse(serializedSystem);
  return Promise.resolve(new IBESystem(d.publicParams, d.privateKey));
};


// ******************************
//
// Key System
//
// ******************************

/**
 * Constructs a new IBESystem instance.
 *
 * @param {Identifier} id - identifier for key
 * @param {number[]} publicParams - parameters for encrypting
 * @param {number[]} privateKey - private key for decrypting
 * @constructor
 */
var IBESystem = function (publicParams, privateKey) {
  this.attributes = new KeyPrototypes.KeyAttributes();
  this.attributes.scheme = SCHEME_NAME;
  this.attributes.canSelect = true;
  this.attributes.canHaveRecipients = true;

  this._setKeyMaterial(publicParams, privateKey);
};

IBESystem.prototype = new KeyPrototypes.KeySystemBase();

/**
 * Sets the ID and key material, and updates the key's fingerprint.
 *
 * @param {Identifier} id - identifier for key
 * @private
 */
IBESystem.prototype._setID = function (id) {
  this.attributes.id = id;
  this._updateFingerprint();
};

/**
 * Sets the public and private key material for this key, and creates the IBE instance.
 *
 * @param publicParams - public IBE parameters.
 * @param privateKey - private key retrieved from key server.
 * @private
 */
IBESystem.prototype._setKeyMaterial = function (publicParams, privateKey) {
  this.publicParams = publicParams;
  this.privateKey = privateKey;

  this.ibe = new IBE(null, this.publicParams);
};

/**
 * Gets the fingerprint for a given ID. If none is supplied, the key's own fingerprint is returned.
 *
 * @param {Identifier} id - object containing the type and value of the ID
 * @returns {string} - fingerprint of ID
 * @private
 */
IBESystem.prototype._getFingerprint = function (id) {
  var fingerprint_data = {
    keyType:      SCHEME_NAME,
    publicParams: this.publicParams,
    id:           (id || this.attributes.id).serialize()
  };

  return Encryptor.hash(JSON.stringify(fingerprint_data));
};

/**
 * Updates the fingerprint.
 * @private
 */
IBESystem.prototype._updateFingerprint = function () {
  this.attributes.fingerprint = this._getFingerprint();
};

/**
 * Checks whether this key system can encrypt for the given ID. Always resolves to true,
 * since any ID can be encrypted for in IBE.
 *
 * @param {Identifier} id - id to check.
 * @promise {Object.<string, *>} - whether we can encrypt for the given ID.
 */
IBESystem.prototype.canEncrypt = function (id) {
  return Promise.resolve({
                           id:         id,
                           canEncrypt: true,
                           message:    ''
                         });
};

/**
 * Performs IBE encryption on a given piece of data.
 *
 * @param {string} data - data to encrypt
 * @param {Identifier} id - id to encrypt for
 * @promise {Object.<string, string>} - result of encryption, including the fingerprint for the receiving side.
 */
IBESystem.prototype.encrypt = function (data, id) {
  try {
    var encrypted = this.ibe.encrypt(JSON.stringify({type: id.type, value: id.value}).toLowerCase(), data);

    return Promise.resolve({
                             encrypted:   JSON.stringify(encrypted),
                             fingerprint: this._getFingerprint(id)
                           });
  } catch (e) {
    return Promise.reject(new Errors.EncryptFailedError('Error during IBE encryption.', e));
  }
};

/**
 * Performs IBE decryption.
 *
 * @param {string} data - data to be decrypted
 * @promise {string} - result of decryption
 */
IBESystem.prototype.decrypt = function (data) {
  try {
    data = JSON.parse(data);
    return Promise.resolve(this.ibe.decrypt(this.privateKey, data));
  } catch (e) {
    return Promise.reject(new Errors.DecryptFailedError('Error during IBE decryption.', e));
  }
};

// TODO - needs symmetric IBE decrypt/encrypt?

/**
 * Performs IBE message signing.
 *
 * Our current IBE system does not allow messages to be signed.
 * Resolves to an empty string.
 *
 * @param {string} data - data to be signed
 * @promise {string} - empty signature string
 */
IBESystem.prototype.sign = function (data) {
  return Promise.resolve('');
};

/**
 * Performs IBE signature verification.
 *
 * Our current IBE system does not allow messages to be signed.
 * Always resolves to true.
 *
 * @param {string} data - data to verify signature for
 * @param {string} signature - signature to verify
 * @resolve {boolean} - whether verification succeeded
 */
IBESystem.prototype.verify = function (data, signature) {
  return Promise.resolve(true);
};

module.exports = new IBEScheme();
