/**
 * Module for handling key management tasks. Anything the overlay might ask the key manager to do,
 * this module can handle. Tasks are organized as functions, assigned to the exports object under
 * the appropriate event name.
 *
 * This module is required as part of the worker thread. It is also required as part of the 'standalone'
 * key manager overlay, which would run all of this code in the main origin thread. The standalone overlay
 * is only used if worker threads are not supported.
 */

var LocalEventEmitter = new (require('events'))();

var Encryptor  = require('../../../common/encryptor'),
    Errors     = require('../../../common/errors'),
    EventNames = require('../event-names'),
    KeyManager = require('../../key-manager'),
    Identifier = require('../../key-prototypes').Identifier,
    UUID       = require('../../../common/uuid');


module.exports = {};

/**
 * Encrypts arbitrary UTF8 text with a given encryption key.
 *
 * @param task.contents - message contents to encrypt.
 * @param task.key      - key used to encrypt contents.
 * @returns Promise     - encrypted data as string.
 */
module.exports[EventNames.ENCRYPT_DATA] = function (task) {
  var messageContents = task.contents,
      messageKey      = task.key;

  return Promise.resolve(Encryptor.encrypt(messageContents, messageKey));
};

/**
 * Decrypts data encrypted through the ENCRYPT_DATA task.
 *
 * @param task.contents - ciphertext to decrypt.
 * @param task.key      - key used to decrypt ciphertext.
 * @returns Promise     - decrypted data as string.
 */
module.exports[EventNames.DECRYPT_DATA] = function (task) {
  var messageContents = task.contents,
      messageKey      = task.key;

  return Promise.resolve(Encryptor.decrypt(messageContents, messageKey));
};

/**
 * Encrypts arbitrary UTF8 text with the draft encryption key.
 *
 * @param task.contents - message contents to encrypt.
 * @returns Promise     - encrypted data as string.
 */
module.exports[EventNames.ENCRYPT_DRAFT] = function (task) {
  var messageContents = task.contents;

  return KeyManager.encryptDraft(messageContents);
};

/**
 * Decrypts data encrypted through the ENCRYPT_DRAFT task.
 *
 * @param task.contents - ciphertext to decrypt.
 * @returns Promise     - decrypted data as string.
 */
module.exports[EventNames.DECRYPT_DRAFT] = function (task) {
  var messageContents = task.contents;

  return KeyManager.decryptDraft(messageContents);
};

/**
 * Encrypts a message key for an array of IDs, using a specified fingerprint.
 *
 * @param task.fingerprint - fingerprint of key to be used to encrypt the message key.
 * @param task.messageKey  - message key to be encrypted.
 * @param task.ids         - array of serialized Identifiers to encrypt the message key for.
 * @returns Promise        - array of encrypted message keys, one for each identifier.
 */
module.exports[EventNames.ENCRYPT_KEY] = function (task) {
  var fingerprint = task.fingerprint;
  var messageKey = task.messageKey;
  var ids = task.ids.map(function (id) {
    return Identifier.parse(id);
  });

  return KeyManager.getKeySystem(fingerprint).then(function (key) {

    // If this key has an ID, add it to the list of recipients,
    // so the sender can decrypt this message later on.
    if (key.attributes.id) {
      ids.push(key.attributes.id);
    }

    return Promise.all(ids.map(function (id) {

      // Sign the message key, then encrypt the key and signature.

      return key.sign(messageKey).then(function (signature) {
        var toEncrypt = JSON.stringify({
                                         data:      messageKey,
                                         signature: signature
                                       });

        return key.encrypt(toEncrypt, id);
      }).then(function (result) {
        return {
          result: result
        };
      }).catch(function (error) {
        return {
          error: error
        };
      });
    })).then(function (results) {
      var result = [],
          errors = [];

      results.forEach(function (item) {
        if (item.result) {
          result.push(item.result);
        } else if (item.error) {
          errors.push(item.error);
        }
      });

      var allErrorsAreRecipientMustInstall = errors.every(function (error) {
        return error instanceof Errors.RecipientMustInstallError;
      });

      if (errors.length && allErrorsAreRecipientMustInstall) {
        var addresses = errors.map(function (error) {
          return error.details.destinationAddress;
        });

        throw new Errors.RecipientMustInstallError('Recipients must install.', {destinationAddress: addresses});
      } else if (errors.length) {
        throw errors[0];
      } else {
        return result;
      }
    });
  });
};

/**
 * Decrypts a message key using the given fingerprint. Resolves with the decrypted key,
 * and the attributes of the key that decrypted it.
 *
 * @param task.fingerprint         - fingerprint of key that should be used to decrypt the message key.
 * @param task.encryptedMessageKey - encrypted message key that needs to be decrypted.
 * @returns Promise                - {keyMaterial, keyAttributes} - decrypted key,
 *                                   and attributes of key that decrypted it.
 */
module.exports[EventNames.DECRYPT_KEY] = function (task) {
  var fingerprint = task.fingerprint;
  var encryptedMessageKey = task.encryptedMessageKey;

  return KeyManager.getKeySystem(fingerprint).then(function (key) {

    // Decrypt the encrypted key, then verify the attached signature.

    return key.decrypt(encryptedMessageKey).then(function (decryptedData) {
      var decrypted = JSON.parse(decryptedData);

      return key.verify(decrypted.data, decrypted.signature).then(function (valid) {
        if (!valid) {
          throw new Errors.VerificationFailedError();
        } else {
          return {
            keyMaterial:   decrypted.data,
            keyAttributes: key.attributes.serialize()
          };
        }
      });
    });
  });
};

/**
 * Verifies that a given fingerprint can encrypt for the given array of Identifiers.
 *
 * @param task.fingerprint - fingerprint of key to test.
 * @param task.ids         - array of serialized Identifiers to test against the specified key.
 * @returns Promise        - array of results from key.canEncrypt
 */
module.exports[EventNames.CAN_ENCRYPT] = function (task) {
  var fingerprint = task.fingerprint;
  var ids = task.ids.map(function (id) {
    return Identifier.parse(id);
  });

  return KeyManager.getKeySystem(fingerprint).then(function (key) {
    return Promise.all(ids.map(function (id) {
      return key.canEncrypt(id).then(function (result) {
        result.id = result.id.serialize();
        return result;
      });
    }));
  });
};

/**
 * Retrieves an array of all key attributes.
 *
 * @returns Promise - array of serialized KeyAttributes.
 */
module.exports[EventNames.GET_KEY_INFO] = function () {
  return KeyManager.getKeyAttributes().then(function (attributesArray) {
    return attributesArray.map(function (attributes) {
      return attributes.serialize();
    });
  });
};

/**
 * Ensures that the master password is present. If not, an error is thrown.
 */
module.exports[EventNames.ENSURE_MASTER_PASSWORD_PRESENT] = function () {
  return KeyManager.ensureKeyStoragePasswordPresent();
};

/**
 * Initializes all key schemes. Ensures that multiples calls to initializeKeys don't result in
 * multiple calls into the key manager, through the use of a local event emitter.
 */
module.exports[EventNames.INITIALIZE_KEYS] = (function () {
  var initializeAnnounceUUID = null;

  return function (task) {
    return new Promise(function (resolve, reject) {
      // If we're currently initializing keys, just wait for it to complete.
      if (initializeAnnounceUUID) {
        LocalEventEmitter.once(initializeAnnounceUUID, function (e) {
          if (e) {
            reject(e);
          } else {
            resolve();
          }
        });
      } else {

        // Generate a random UUID so future calls during this invocation can wait on this
        // to complete.
        initializeAnnounceUUID = UUID();

        KeyManager.initializeKeys().then(function () {

          // Let all the other requesters know that we're ready now.
          LocalEventEmitter.emit(initializeAnnounceUUID);
          initializeAnnounceUUID = null;

        }).then(resolve).catch(function (e) {
          LocalEventEmitter.emit(initializeAnnounceUUID, e);
          initializeAnnounceUUID = null;

          reject(e);
        });
      }
    });
  };
})();

/**
 * Simple heartbeat function that returns true. Used to test whether the key manager is alive.
 * @returns Promise - true
 */
module.exports[EventNames.HEARTBEAT] = function () {
  return Promise.resolve(true);
};
