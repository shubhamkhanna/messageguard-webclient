/**
 * Registers handlers for key manager task names. Uses a localStorage event emitter.
 *
 * In order to keep multiple windows from interfering with each other, the current overlay's
 * windowUUID parameter is used to keep events from crossing over into a different window.
 *
 * Tasks are issued as events fired from key-management/communicator/emitter.js
 */

var Errors       = require('../../../common/errors'),
    urls         = require('../../../common/urls'),
    EventEmitter = require('../../../common/storage/storage-objects').eventEmitter;

var options    = urls.getUrlOptions(),
    windowUUID = options.window_uuid || '';

function registerStorageEventHandler(eventName, handler) {

  // Prefix the eventName with the windowUUID to keep events within the current window.

  EventEmitter.on(windowUUID + '_' + eventName, function (e) {
    var task = e.details;

    /**
     * Wraps an error within an MGError if needed, then emit on the replyUUID.
     */
    var catcher = function (e) {
      if (!(e instanceof Errors.MGError)) {
        e = new Errors.MGError(e.toString());
      }

      EventEmitter.emit(task.replyUUID, {err: e.serialize()});
    };

    // Try-catch used in case the handler throws an error
    // outside a promise.
    try {
      handler(task).then(function (result) {
        EventEmitter.emit(task.replyUUID, {result: result});
      }).catch(catcher);
    } catch (e) {
      catcher(e);
    }
  });
};

module.exports = {
  registerHandler: registerStorageEventHandler
};
