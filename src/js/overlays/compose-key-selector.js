var KeyManager = require('../key-management/communicator/emitter'),
    $          = require('../common/jquery-bootstrap');

function DropDown(el, cb) {
  this.dd = el;
  this.cb = cb;
  this.initEvents();
}
DropDown.prototype = {
  initEvents: function () {
    var obj = this;

    $(obj.dd).find('.selected-option').on('click', function (event) {
      $(obj.dd).toggleClass('active');
      event.stopPropagation();
    });

    $(obj.dd).delegate('.dropdown li', 'click', function () {
      $(obj.dd).find('.selected-option').html($(this).find('a').html());
      obj.cb(this);
    });
  }
};

// TODO - this doesn't seem to belong. Probably should be in some kind of init function that is intentionally called
$(document).click(function () {
  $('#key-selector').removeClass('active');
});

KeyManager.getKeyInfo().then(function (attributesArray) {
  for (var i = 0; i < attributesArray.length; i++) {
    var keyItem = $('<li><a href="#"><i class="fa fa-key key-icon"></i><span class="key-name"></span></a></li>');
    keyItem.find('.key-icon').css('color', attributesArray[i].color);
    keyItem.find('.key-name').text(attributesArray[i].name);
    keyItem.data('key-id', attributesArray[i].id);
    $('#key-selector .dropdown').append(keyItem);
  }
});
