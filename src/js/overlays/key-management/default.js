var receiver = require('../../key-management/communicator/receiver/forwarder'),
    urls     = require('../../common/urls');

if (window.Worker) {
  receiver.init();
} else {
  // This overlay loads with only enough "know-how" to forward tasks to a
  // key manager worker thread. If we can't use worker threads, we need to
  // reload as the standalone version. We do this so we don't unnecessarily
  // load all the key management code twice, since most browsers support
  // worker threads.

  var urlOptions = urls.getUrlOptions();
  window.location = urls.getOverlayUrl('key-management/standalone-key-manager.html', urlOptions);
}
