/**
 * Generic compose class
 * @module
 * @requires bower_components/tether
 * @requires common/jquery-bootstrap
 * @requires overlays/overlay-base
 * @requires common/message-types
 * @requires overlays/wysiwyg
 */

"use strict";

var $           = require('../common/jquery-bootstrap'),
    Errors      = require('../common/errors'),
    MessageType = require('../common/message-type'),
    OverlayBase = require('./overlay-base'),
    Storage     = require('../common/storage'),
    Identifier  = require('../key-management/key-prototypes').Identifier,
    sanitizer   = require('../common/html-sanitizer');

require('./bootstrap-wysiwyg');

var SmallState = {
  COMPOSE:          "compose",
  ENTER_RECIPIENTS: "enter_recipients",
  KEY_SELECT:       "key-select"
};

/**
 * Controls the generic compose interface
 * @constructor
 * @alias module:overlays/generic-compose
 */
function GenericCompose() {
  this.linkMask = $('#linkMask');
  this.linkEntry = $('#linkEntry');
  this.linkText = $('#linkText');
  this.linkURL = $('#linkURL');
  this.linkOK = $('#linkOK');
  this.smallState = SmallState.COMPOSE;
  this.selectedKeyIndex = -1;
  this.formattingShown = false;
  this.includeAddKeyBtn = true;
  this.canSaveDraft = true;
}

GenericCompose.prototype = new OverlayBase();
GenericCompose.prototype.constructor = GenericCompose;

GenericCompose.prototype.showLinkEntry = function (textToDisplay, URL) {
  this.linkMask.show();
  this.linkEntry.show();

  this.updateTextToUrl = false;
  this.linkText.val(textToDisplay || '');
  this.linkURL.val(URL || '').trigger('input');
};

GenericCompose.prototype.getSelectedLinkTag = function () {
  var selection = window.getSelection();
  if (selection.anchorNode === null) {
    return null;
  }
  var node = selection.anchorNode.nodeType === 3 ? selection.anchorNode.parentNode : selection.anchorNode;
  var nodeIsAnchor = node.tagName.toUpperCase() === 'A';
  if (nodeIsAnchor || $(node).parents('a').length > 0) {
    return nodeIsAnchor ? node : $(node).closest('a')[0];
  } else {
    return null;
  }
};

GenericCompose.prototype.linkFunction = function (textToDisplay, URL) {
  var selection = window.getSelection();

  // Update the text.
  if (textToDisplay !== this.linkCurrentText) {
    this.linkSelection.deleteContents();
    this.linkSelection.insertNode(document.createTextNode(textToDisplay));
  }

  // Sanitize the URL.
  if (!/^(https?:\/\/|mailto:)/.test(URL)) {
    URL = 'http://' + URL;
  }

  // If we are editing a URL we behave differently.
  if (this.linkCurrentNode !== null) {
    this.linkCurrentNode.href = URL;
  } else {
    this.getEditor().focus();
    selection.removeAllRanges();
    selection.addRange(this.linkSelection);
    document.execCommand('createLink', true, URL);
  }

  selection.removeAllRanges();
};

/**
 * Initialize and begin operating
 */
GenericCompose.prototype.init = function () {
  OverlayBase.prototype.init.call(this);

  this.uploadTarget = $('#upload').data('target');

  var self = this;
  this.editor = this.getEditor();

  this.registerMessageHandler(MessageType.OVERLAY_READY, false, function (data) {
    self.postMessage(MessageType.GET_CONTENTS);
  });

  if (this.includeAddKeyBtn) {
    $("#small-add-key").css('display', 'block');
    $("#small-add-key-separator").css('display', 'block');
    $("#large-add-key").css('display', 'block');
    $("#large-add-key-separator").css('display', 'block');
  }


  // Setup the editor.
  var fonts = [
    ['Helvetica', '\'Helvetica Neue\', Helvetica, Arial, sans-serif'],
    ['Sans Serif', 'arial, helvetica, sans-serif'],
    ['Serif', '\'times new roman\', serif'],
    ['Fixed Width', 'monospace, monospace'],
    ['Wide', '\'arial black\', sans-serif'],
    ['Narrow', '\'arial narrow\', sans-serif'],
    ['Comic Sans MS', '\'comic sans ms\', sans-serif'],
    ['Garamond', 'garamond, serif'],
    ['Georgia', 'georgia, serif'],
    ['Tahoma', 'tahoma, sans-serif'],
    ['Trebuchet MS', '\'trebuchet ms\', sans-serif'],
    ['Verdana', 'verdana, sans-serif']
  ];
  $.each(fonts, function (idx, fontItem) {
    $('#font-dropup-btn')
        .next('.dropdown-menu')
        .append($('<li><a data-edit="fontName ' + fontItem[1] + '" data-font="' + fontItem[1] + '"><font face="' +
                  fontItem[1] + '">' + fontItem[0] + '</font></a></li>'));
  });

  this.editor.wysiwyg({
                        fileUploadError: function (reason, detail) {
                          var msg = '';
                          if (reason === 'unsupported-file-type') {
                            msg = 'The selected file was not a supported image.';
                          } else {
                            msg = 'Error uploading file: ' + reason + ', ' + detail;
                          }
                          $(document.body).mask(msg, 'OK');
                        }
                      });

  // Add tooltips
  $('[title]').tooltip({
                         container: 'body'
                       }).on('click', function () {
    $(this).tooltip('hide');
  });

  // Add an option for speech input.
  if ("onwebkitspeechchange" in document.createElement("input")) {
    var editorOffset = this.editor.offset();
    $('#voiceBtn').css('position', 'fixed').offset({
                                                     top:  editorOffset.top,
                                                     left: editorOffset.left + this.editor.innerWidth() - 35
                                                   });
  } else {
    $('#voiceBtn').hide();
  }

  this.mutating = false;

  var self = this;

  this.observer = new MutationObserver(function (mutations) {

    if (self.mutating) {
      self.mutating = false;
      return;
    }

    self.mutating = true;

    mutations.forEach(function (mutation) {
      if ($('#large').css('display') != 'none') {
        $("#small-editor").html($('#large-editor').html());
      }
      else {
        $("#large-editor").html($('#small-editor').html());
      }
    });
  });

  var observerConfig = {
    childList:     true,
    subtree:       true,
    characterData: true
  };

  var targetNode = document.getElementById('large-editor');
  this.observer.observe(targetNode, observerConfig);
  targetNode = document.getElementById('small-editor');
  this.observer.observe(targetNode, observerConfig);

  var addKeyLinkLarge = this.getAddKeyLink('encrypt', {fullKeyOptions: true}, this.addKeyCallback);
  var addKeyLinkSmall = this.getAddKeyLink('encrypt', {fullKeyOptions: true}, this.addKeyCallback);

  $('#large-add-key').append(addKeyLinkLarge.linkElement);
  $('#small-add-key').append(addKeyLinkSmall.linkElement);

  Storage.local.getItem('TextToolsEnabled').then(function (textToolsEnabled) {
    this.textToolsEnabled = textToolsEnabled || 'false';

    if (this.textToolsEnabled === 'true') {
      textToolsButton.addClass('active');
      toolbarPopup.show();
    }

    this.bindUIActions();
    this.editor.focus();
    this.updateView();
  }.bind(this));
};

OverlayBase.prototype.addKeyCallback = function () {
  if (err) {
    self.showMaskedMessage(err, {isError: true, isClosable: true});
    return;
  }

  var smallList = document.getElementById('small-key-list'),
      largeList = document.getElementById('large-key-list');

  for (var i = 0; i < this.keyInfo.length; i++) {
    if (this.keyInfo[i].fingerprint == newKeyAttributes.fingerprint) {
      // Somewhat hackish
      $(smallList).add(largeList).children(':eq(' + (i + 2) + ')').click();
      break;
    }
  }
};

/**
 * Signals that the key info has been updated
 */
OverlayBase.prototype.keysUpdated = function () {
  this.addKeysToInterface();
};

/**
 * Gets the contenteditable div element
 */
GenericCompose.prototype.getEditor = function () {
  if ($("#large").css("display") != "none") {
    return $("#large-editor");
  } else {
    return $("#small-editor");
  }
};

/**
 * Sets the content of the small and large editors
 * @param {string} content The content that should be set
 */
GenericCompose.prototype.setEditorContent = function (content) {
  $("#large-editor").html(content);
  $("#small-editor").html(content);
};

/**
 * Gets the content of the editor currently in use
 * @return {string} The content of the editor in use
 */
GenericCompose.prototype.getEditorContent = function () {
  if ($("#large").css("display") != "none") {
    return $("#large-editor").html();
  } else {
    return $("#small-editor").html();
  }
};

/**
 * Gets the recipients string from the large or small recipient input
 * @return {promise} A promise that always resolves with the recipients
 */
GenericCompose.prototype.getRecipients = function () {
  var recipients;
  if ($("#large").css("display") != "none") {
    recipients = $("#large-recipients-input").val();
  } else {
    recipients = $("#small-recipients-input").val();
  }
  recipients = recipients.split(/[,;\s]+/g);
  return Promise.resolve(recipients);
};

/**
 * Sets the color of the large and small key icons
 * @param {string} color A css rgb color string of the form rgb(r,g,b)
 */
GenericCompose.prototype.setKeyIconColor = function (color) {
  //Change dropdown menu button key icon color
  $('#large-key-icon').css('color', color);
  $('#small-key-icon').css('color', color);
};

/**
 * Refreshes the small and large key lists by removing all keys and adding the current list of keys
 */
GenericCompose.prototype.addKeysToInterface = function () {

  var smallList = document.getElementById('small-key-list');
  var largeList = document.getElementById('large-key-list');

  //Clear lists
  while (smallList.children[2]) {
    smallList.removeChild(smallList.children[2]);
  }
  while (largeList.children[2]) {
    largeList.removeChild(largeList.children[2]);
  }

  var self = this;

  var keyItemOnClick = function () {

    var smallList = document.getElementById('small-key-list');
    var largeList = document.getElementById('large-key-list');

    //Remove current highlight
    $(smallList).add(largeList).children().removeClass('key-list-selected-item');

    var newIndex = $(this).index();

    //Account for add key link and divider by subtracting 2
    self.selectedKeyIndex = newIndex - 2;

    //Add highlight to newly selected keys
    $(smallList).add(largeList).children(':eq(' + newIndex + ')').addClass('key-list-selected-item');

    self.setKeyIconColor(self.keyInfo[self.selectedKeyIndex].color);
    $('#large-key-name').text(self.keyInfo[self.selectedKeyIndex].name);
  };

  this.keyInfo.forEach(function (key, i) {

    var keyBase = "<span class=\"fa fa-key\" style=\"color: " + key.color + ";\"></span> - " + key.name;

    var div = document.createElement('div');
    //div.style.width = '100%';
    div.innerHTML = keyBase;
    div.keyTag = key;
    smallList.appendChild(div);
    div.onclick = keyItemOnClick;

    var li = document.createElement('li');
    li.innerHTML = '<a href="#">' + keyBase + '</a>';
    li.keyTag = key;
    largeList.appendChild(li);
    li.onclick = keyItemOnClick;

    if (i == 0) {
      div.className += ' key-list-selected-item';
      li.className += ' key-list-selected-item';
      self.selectedKeyIndex = 0;
      self.setKeyIconColor(self.keyInfo[self.selectedKeyIndex].color);
      $('#large-key-name').text(self.keyInfo[self.selectedKeyIndex].name);
    }
  });

  if (this.selectedKeyIndex != -1) {
    $('#key-dropdown').attr('disabled', false);
    $('#key-button').attr('disabled', false);
    $('#large-key-name').text(this.keyInfo[this.selectedKeyIndex].name);
  }
  else if (!this.includeAddKeyBtn) {
    $('#key-dropdown').attr('disabled', true);
    $('#key-button').attr('disabled', true);
  }
};

/**
 * Shows a message overlay prompting the user to contact the recipient and ask them to install MessageGuard
 */
GenericCompose.prototype.showRequestRecipientInstallMessage = function (destinationAddress) {
  var self = this;

  var promptMessageHTML = 'Whoops! One or more of your recipients hasn\'t installed MessageGuard yet. ' +
                          'Click <a id="prompt-link" href="#">here</a> to send them a message requesting that they install MessageGuard.' +
                          '<br><br>' +
                          'Once your recipient has installed MessageGuard, you will be able to encrypt messages for them. ' +
                          'In the meantime, feel free to close this message and it\'ll be saved as a draft.',
      promptMessage     = $('<span />').append(promptMessageHTML);

  promptMessage.find('#prompt-link').click(function (e) {
    e.preventDefault();
    self.postMessage(MessageType.COMPOSE_INSTALL_PROMPT, destinationAddress);
    return false;
  });

  this.showMaskedMessage(promptMessage, {isError: true, isClosable: true});
};

/**
 * Starts the encryption process by obtaining the list of recipients then calling performEncrytion with the data and
 * recipients.
 */
GenericCompose.prototype.startEncryption = function (keyAttributes) {

  var self = this;

  var encryptSuccess = function (encryptedData) {
    setTimeout(function () {
      self.canSaveDraft = false;
      self.postMessage(MessageType.MESSAGE_ENCRYPTED, encryptedData);
    }, 0);
    //Do not post MESSAGE_ENCRYPTED here
  };

  var encryptError = function (err) {
    if (err instanceof Errors.RecipientMustInstallError) {
      self.showRequestRecipientInstallMessage(err.details.destinationAddress);
      return;
    }

    self.showMaskedMessage(err, {isError: true, isClosable: true});
  };

  if (keyAttributes.canHaveRecipients) {
    this.getRecipients().then(function (recipients) {
      if (recipients && recipients.length > 0) {
        self.performEncryption(keyAttributes, recipients).then(encryptSuccess).catch(encryptError);
      }
      else {
        self.showMaskedMessage("You haven't select any recipients to encrypt this message for.",
                               {isError: true, isClosable: true});
      }
    });
  }
  else {
    self.performEncryption(keyAttributes, []).then(encryptSuccess).catch(encryptError);
  }
};

/**
 * Works through the packager to encrypt the data in the editor using the selected KeyAttributes and given recipients.
 * @param {KeyAttributes} keyAttributes The key attributes being used to encryptError
 * @param {string[]} recipients An array of recipient identifying strings (e.g. email addresses)
 */
GenericCompose.prototype.performEncryption = function (keyAttributes, recipients) {
  var self                      = this,
      minimumEncryptionTime     = 750,
      timePerParticipant        = 750,
      totalRecipientMessageTime = keyAttributes.canHaveRecipients ?
                                  (recipients.length * timePerParticipant) : minimumEncryptionTime,
      timeouts                  = [];

  // Schedule a message to be shown later. Store the timeout so it can be canceled.
  var scheduleMessage = function (message, time) {
    timeouts.push(window.setTimeout(function () { self.showMaskedMessage(message, {showSpinner: true}); }, time));
  };

  // If we have multiple recipients, show an "encrypting for ..." message for each of them.
  if (keyAttributes.canHaveRecipients) {
    // Show the first message immediately.
    if (recipients.length) {
      self.showMaskedMessage('Encrypting for ' + recipients[0] + ' &hellip;', {showSpinner: true});
    }

    // Schedule the remaining messages.
    for (var i = 1; i < recipients.length; i++) {
      scheduleMessage('Encrypting for ' + recipients[i] + ' &hellip;', i * timePerParticipant);
    }
    scheduleMessage('Finishing encryption &hellip;', totalRecipientMessageTime);
  } else {
    self.showMaskedMessage('Encrypting message &hellip;', {showSpinner: true});
  }

  // Show progress dialog for a minimum time before sending the message.
  var encryptingMessageDialogsPromise = new Promise(function (resolve) {
    window.setTimeout(resolve, Math.max(minimumEncryptionTime, totalRecipientMessageTime));
  });

  // Encrypt the messasge on another thread.
  var encryptMessagePromise = this.packager.encrypt(
      sanitizer.sanitize(self.getEditorContent()),
      keyAttributes,
      recipients.map(function (recipient) { return new Identifier(recipient, 'Email'); })
  );

  return Promise.all([encryptingMessageDialogsPromise, encryptMessagePromise]).then(function (results) {
    //return results[1];

    return new Promise(function (resolve) {
      // Fade in the ciphertext. Feature needs more testing.
      var encryptedContent = results[1];

      self.showMaskedMessage(encryptedContent, {
        contentWrapper: '<div style="display: none; margin: 10px; line-height: 1.5; font-size: 14px; word-wrap: break-word;" id="fade-in-ciphertext" />'
      });
      $('#fade-in-ciphertext').fadeIn(750);

      window.setTimeout(function () {
        resolve(encryptedContent);
      }, 750);
    });
  }).catch(function (err) {
    // Clear the messages we would show about encryption as we have hit an error.
    timeouts.forEach(function (timeout) { window.clearTimeout(timeout); });
    throw err;
  });
}
;

/**
 * Works through the packager to decrypt the given encrypted data.
 * @param {string} encryptedData The data to be decrypted
 */
GenericCompose.prototype.performDecryption = function (encryptedData) {
  var self = this;

  this.showMaskedMessage('Decrypting message &hellip;', {showSpinner: true});

  // First try decrypting as if it were a draft, using the static draft decryption method.
  // If that fails, try our keys.
  return self.decryptDraft(encryptedData).then(function (decryptedDraft) {
    self.hideMaskedMessage();
    self.setEditorContent(decryptedDraft);
    return decryptedDraft;
  }).catch(function (e) {
    if (!(e instanceof Errors.EncryptionError)) {
      throw e;
    }

    return self.packager.decrypt(encryptedData).then(function (result) {
      self.setKeyIconColor(result.keyAttributes.color);

      return sanitizer.sanitize(result.data);
    }).catch(function (error) {
      if (error instanceof Errors.NoMatchingFingerprintsError) {
        var result = self.getNoKeyAvailableMessage({
                                                     scheme:        error.details.scheme,
                                                     fingerprints:  error.details.messageFingerprints,
                                                     forEncryption: false
                                                   });
        self.showMaskedMessage(result.messageElement, {isError: true});
        return result.promise.then(function () {
          return self.performDecryption(encryptedData);
        });
      }
      else {
        switch (error.name) {
          case Errors.MalformedMessageError.name:
            self.showMaskedMessage(
                'This message is malfored. Please contact the sencder and ask them to resend it.',
                {isError: true, isClosable: true});
            break;

          case Errors.AllDecryptAttemptsFailedError.name:
            self.showMaskedMessage("You don't have permission to decrypt this message.", {isError: true});
            break;
          default:
            self.showMaskedMessage(error, {isError: true});
        }
      }
    });
  });
};

/**
 * Updates the contenteditable div element to utilize full height in large mode
 * and full width in small mode
 */
GenericCompose.prototype.updateView = function () {

  //Only update editor view if showing LARGE view

  if ($("#large").css("display") != "none") {
    this.editor = $("#large-editor");
    // Update the editor.
    var height;
    if ($('#large-finalize-bar').css('display') == 'none') {
      height = $('body').height() - this.editor.offset().top;
    } else {
      height = $('#large-finalize-bar').offset().top - this.editor.offset().top;
    }
    this.editor.css('min-height', height + 'px');
    this.editor.css('max-height', height + 'px');

    // Make sure the carrot or selection is still visible.
    var selection = window.getSelection();
    if (selection.anchorNode !== null) {
      var node = selection.anchorNode.nodeType === 3 ? selection.anchorNode.parentNode : selection.anchorNode;
      node.scrollIntoViewIfNeeded();
    }

    // Adjust other elements as needed.
    $('.scrollable-menu').css('max-height', this.editor.height() + 'px');
  }
  else {
    this.editor = $("#small-editor");

    this.editor.css("left", parseFloat($('#small-recipients-container').width()) + 5 + 'px');
    // Update the editor.
    var width = $('#small-finalize-container').offset().left - this.editor.offset().left - 5;
    this.editor.css('min-width', width + 'px');
    this.editor.css('max-width', width + 'px');

    var height = $("#small").height() + 20;
    this.editor.css('min-height', height + 'px');
    this.editor.css('max-height', height + 'px');

    var topPadding = ($('#small').height() - 19) / 2;
    this.editor.css('padding-top', topPadding + 'px');

    var smallHeight = $("#small").height() - 3;
    $("#small-key-list").css('min-height', smallHeight + 'px');
    $("#small-key-list").css('max-height', smallHeight + 'px');

    // Make sure the carrot or selection is still visible.
    var selection = window.getSelection();
    if (selection.anchorNode !== null) {
      var node = selection.anchorNode.nodeType === 3 ? selection.anchorNode.parentNode : selection.anchorNode;
      node.scrollIntoViewIfNeeded();
    }

    // Adjust other elements as needed.
    $('.scrollable-menu').css('max-widtht', this.editor.width() + 'px');
  }
};

GenericCompose.prototype.hideLinkEntry = function () {
  this.linkMask.hide();
  this.linkEntry.hide();
};

/**
 * Handles the encrypt button click event by obtaining the currently selected key attributes
 * and using them to start the encryption process.
 */
GenericCompose.prototype.encryptClickAction = function () {
  var self = this;

  if (this.selectedKeyIndex == -1) {
    var message = this.getNoKeyAvailableMessage({forEncryption: true});

    this.showMaskedMessage(message.messageElement, {isError: true, isClosable: true});
    message.promise.then(function (keyAttributes) {
      self.startEncryption(keyAttributes);
    }).catch(function (err) {
      self.showMaskedMessage(err, {isError: true, isClosable: true});
    });
  }
  else {
    this.startEncryption(this.keyInfo[this.selectedKeyIndex]);
  }
};

/**
 * Handles the editing button click event by opening or closing the rich text editing bar.
 */
GenericCompose.prototype.editingClickAction = function () {
  if (this.formattingShown) {
    document.getElementById('large-format-bar').style.display = 'none';
  } else {
    document.getElementById('large-format-bar').style.display = 'inline';
  }
  this.formattingShown = !this.formattingShown;
  this.updateView();
};

/**
 * Handles the cancel click button event by sending the editor content to the overlay manager with
 * a CANCEL message.
 */
GenericCompose.prototype.cancelClickAction = function () {
  this.postMessage(MessageType.CANCEL, this.getEditorContent());
};

/**
 * Handles the recipient button click event on the small view by showing or hiding the
 * recipients input box depending on the current state of the small view.
 * IF smallState == COMPOSE - Hide editor and finalize bar, show recipients input
 * IF smallState == ENTER_RECIPIENTS - Show editor and finalize bar, hide recipients input
 * IF smallState == KEY_SELECT - Show recipients input, hide key list bar
 */
GenericCompose.prototype.recipientClickAction = function () {
  if (this.smallState == SmallState.COMPOSE) {
    $('#small-editor').css('display', 'none');
    $('#small-finalize-container').css('display', 'none');
    $('#small-to').css('display', 'block');
    this.smallState = SmallState.ENTER_RECIPIENTS;
  }
  else if (this.smallState == SmallState.ENTER_RECIPIENTS) {
    $('#small-editor').css('display', 'block');
    $('#small-finalize-container').css('display', 'block');
    $('#small-to').css('display', 'none');
    this.smallState = SmallState.COMPOSE;
  }
  else if (this.smallState == SmallState.KEY_SELECT) {
    $('#small-to').css('display', 'block');
    $('#small-key-list').css('display', 'none');
    this.smallState = SmallState.ENTER_RECIPIENTS;
  }
};

/**
 * Handles the key button click event on the small view by showing or hiding the
 * key list depending on the current state of the small view.
 * IF smallState == COMPOSE - Show key list
 * IF smallState == ENTER_RECIPIENTS - Hide recipients input, show finalize bar and key list
 * IF smallState == KEY_SELECT - Hide key list
 */
GenericCompose.prototype.smallViewKeyBtnClickAction = function (event) {
  if (this.smallState == SmallState.COMPOSE) {
    $('#small-key-list').css('display', 'block');
    this.smallState = SmallState.KEY_SELECT;
  }
  else if (this.smallState == SmallState.ENTER_RECIPIENTS) {
    $('#small-to').css('display', 'none');
    $('#small-finalize-container').css('display', 'block');
    $('#small-key-list').css('display', 'block');
    this.smallState = SmallState.KEY_SELECT;
  }
  else if (this.smallState == SmallState.KEY_SELECT) {
    $('#small-key-list').css('display', 'none');
    this.smallState = SmallState.COMPOSE;
  }
};

GenericCompose.prototype.linkClickAction = function () {
  var selection = window.getSelection();
  if (!selection.rangeCount) {
    return;
  }

  this.linkCurrentNode = getSelectedLinkTag();
  var linkCurrentURL = '';
  if (this.linkCurrentNode !== null) {
    linkCurrentURL = this.linkCurrentNode.href || '';
    var range = document.createRange();
    range.selectNodeContents(this.linkCurrentNode);
    selection.removeAllRanges();
    selection.addRange(range);
  }

  this.linkCurrentText = selection.toString();
  this.linkSelection = selection.getRangeAt(0);
  this.showLinkEntry(this.linkCurrentText, linkCurrentURL);
};

GenericCompose.prototype.unlinkClickAction = function () {
  var node = getSelectedLinkTag();
  if (node !== null) {
    var range = document.createRange();
    range.selectNode(node);
    var selection = window.getSelection();

    selection.removeAllRanges();
    selection.addRange(range);

    this.getEditor().focus();
    document.execCommand('unlink');

    selection.removeAllRanges();
  }
};

GenericCompose.prototype.linkOKClickAction = function () {
  this.hideLinkEntry();
  linkFunction(linkText.val(), linkURL.val());
};

GenericCompose.prototype.uploadTargetClickAction = function () {
  document.getElementById('upload').click();
};

GenericCompose.prototype.cancelLinkClick = function () {
  this.hideLinkEntry();
};

GenericCompose.prototype.textToolsButtonClickAction = function () {
  if (this.textToolsEnabled === 'true') {
    this.textToolsEnabled = 'false';
    textToolsButton.removeClass('active');
    toolbarPopup.hide();
  } else {
    this.textToolsEnabled = 'true';
    textToolsButton.addClass('active');
    toolbarPopup.show();
  }

  Storage.local.setItem('TextToolsEnabled', this.textToolsEnabled);
  this.updateView();
};

GenericCompose.prototype.linkTextPasteAction = function () {
  if (linkText.val() !== linkURL.val()) {
    this.updateTextToUrl = false;
  }
};

GenericCompose.prototype.linkURLPasteAction = function () {
  if (linkText.val() === '' || this.updateTextToUrl) {
    this.updateTextToUrl = true;
    linkText.val(linkURL.val());
  }
  if (linkURL.val() === '') {
    linkOK.attr('disabled', true);
  } else {
    linkOK.attr('disabled', false);
  }
};

var linkTextKeyPressAction = function (e) {
  if (e.keyCode === 13) {
    linkOK.click();
    return false;
  }
};

var documentKeyUp = function (e) {
  if (e.keyCode === 27) {
    this.hideLinkEntry();
    return false;
  }
};

/**
 * Binds UI events to the approriate actions. i.e. Encrypt click to
 * encryptClikcAction function.
 */
GenericCompose.prototype.bindUIActions = function () {

  // Use "self" to access properties and functions of this object inside of an event listener, as within the
  // listener "this" refers to the object being acted upon, not GenericCompose.
  var self = this;

  $('#small').mouseup(function (e) {
    var container = $("#small-key-list");
    var keyButton = $("#key-button");

    if (!container.is(e.target) && container.has(e.target).length === 0 && !keyButton.is(e.target) &&
        keyButton.has(e.target).length === 0 &&
        self.smallState === SmallState.KEY_SELECT) {
      $('#small-key-list').css('display', 'none');
      self.smallState = SmallState.COMPOSE;
    }
  });

  // ------------------------------------------------------------
  // -- Click events
  // ------------------------------------------------------------
  $('.encrypt-btn').click(this.encryptClickAction.bind(this));
  $('#editing').click(this.editingClickAction.bind(this));
  $('#to-button').click(this.recipientClickAction.bind(this));
  $('#key-button').click(this.smallViewKeyBtnClickAction.bind(this));
  $('.cancel-btn').click(this.cancelClickAction.bind(this));
  $('#linkButton').click(this.linkClickAction.bind(this));
  $('#unlinkButton').click(this.unlinkClickAction.bind(this));
  this.linkOK.on('click', this.linkOKClickAction.bind(this));
  $(this.uploadTarget).on('click', this.uploadTargetClickAction.bind(this));
  $('#linkCancel,#linkCancelCorner').on('click', this.cancelLinkClick.bind(this));
  textToolsButton.on('click', this.textToolsButtonClickAction.bind(this));

  // ------------------------------------------------------------
  // -- Paste events
  // ------------------------------------------------------------
  this.linkText.on('input propertychange paste', this.linkTextPasteAction);
  this.linkURL.on('input propertychange paste', this.linkURLPasteAction);

  // ------------------------------------------------------------
  // -- Keypress events
  // ------------------------------------------------------------
  this.linkText.add(this.linkURL).on('keypress', this.linkTextKeyPressAction);
  $(document).on('keyup', this.documentKeyUp);

  // ------------------------------------------------------------
  // -- Resize events
  // ------------------------------------------------------------
  $(window).resize(this.updateView);
};

var genericCompose = new GenericCompose();
// Handle the text tools popup. ???
var toolbarPopup = $('#toolbarPopup');
var textToolsButton = $('#textToolsButton');

module.exports = GenericCompose;
