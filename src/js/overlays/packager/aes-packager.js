/**
 * Generic compose class
 * @module
 * @requires packager/packager-base
 * @requires common/encoder
 */

"use strict";

var KeyManager   = require('../../key-management/communicator/emitter'),
    PackagerBase = require('./packager-base'),
    encoder      = require('../../common/encoder'),
    Encryptor    = require('../../common/encryptor'),
    Errors       = require('../../common/errors');

/**
 * Controls the generic compose interface
 * @constructor
 * @alias module:overlays/generic-compose
 */
function AESPackager() {

}

AESPackager.prototype = new PackagerBase();
AESPackager.prototype.constructor = AESPackager;

/**
 * Initialize
 */
AESPackager.prototype.init = function () {

};

/**
 * Encrypts the given message using AES
 * @param {string} message. The message to encrypt.
 */
AESPackager.prototype.encryptData = function (message) {
  var self = this;

  return new Promise(function (resolve) {
    var key = self._generateKey();

    KeyManager.encryptData(message, key).then(function (encryptedMessage) {
      resolve({
                encrypted:   encryptedMessage,
                keyMaterial: key
              });
    });
  });
};

/**
 * Decrypts the given details using AES
 * @param {string} details. The details to decrypt. This details do not contain an '|mg|'
 */
AESPackager.prototype.decryptData = function (details) {
  return new Promise(function (resolve) {
    var key = details.keyMaterial;

    KeyManager.decryptData(details.encrypted, key).then(resolve).catch(function (e) {
      if (e instanceof Errors.EncryptionError) {
        throw new Errors.PackagerError('Unable to decrypt the message container.');
      } else {
        throw e;
      }
    });
  });
};

/**
 * Checks on the packager
 */
AESPackager.prototype.check = function (data) {

};

/**
 * Generates an AES encryption keySize
 */
AESPackager.prototype._generateKey = function () {
  return Encryptor.randomKey();
};

module.exports = AESPackager;
