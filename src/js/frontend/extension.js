/**
 * @file Sets up the environment for the extension and then runs the front end.
 * @requires frontend/main
 */
"use strict";

require('./main');