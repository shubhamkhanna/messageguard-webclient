/**
 * Created by Scott Ruoti on 4/27/2016.
 */

var MouseEvents = {};
var jQuery = require('jquery');

/**
 * Simulates a mouse click.
 * @method simulateMouseClick
 */
MouseEvents.simulateMouseClick = function (target) {
  if (!(target instanceof Element) && target instanceof jQuery && target.length) {
    target = target[0];
  }

  this.performMouseAction(target, "mousedown");
  this.performMouseAction(target, "mouseup");
  this.performMouseAction(target, "click");
};

/**
 * Perfoms a mouse aciton.
 * @method performMouseAction
 */
MouseEvents.performMouseAction = function (target, eventName) {
  if (target == null) {
    return;
  }

  var event = target.ownerDocument.createEvent("MouseEvents");
  event.initMouseEvent(eventName, true, // can bubble
                       true, // cancellable
                       document.defaultView, 1, // clicks
                       0, 0, // screen coordinates
                       0, 0, // client coordinates
                       false, false, false, false, // control/alt/shift/meta
                       0, // button,
                       null);
  target.dispatchEvent(event);
};

module.exports = MouseEvents;