/**
 * Controller that can be used with Gmail.
 * @module
 * @requires frontend/controller-base
 * @requires frontend/generic/read-overlay-manager
 * @requires frontend/generic/compose-overlay-manager
 */
"use strict";

var ControllerBase        = require('../../controller-base'),
    ReadOverlayManager    = require('./read-overlay-manager'),
    ComposeOverlayManager = require('./compose-overlay-manager');

var jQuery = require('jquery');

var PackageWrapper = require('./package-wrapper'),
    Tutorial       = require('./tutorial');

var styleString = require('fs').readFileSync(__dirname + '/../../../../../build/staging/sass/css/messageguard.css',
                                             'utf8');

/**
 * Creates a controller suitable for gmail.
 * @constructor
 * @extends module:frontend/controller-base
 * @alias module:frontend/generic/controller
 */
function Controller() {
  // Insert a style sheet into the head.
  var style = document.createElement('style');
  style.type = 'text/css';
  if (style.styleSheet) {
    style.styleSheet.cssText = styleString;
  } else {
    style.appendChild(document.createTextNode(styleString));
  }
  (document.head || document.getElementsByTagName('head')[0]).appendChild(style);

  ControllerBase.call(this, '.I5', undefined, 'div[name="encryptedEmail"]');

  // Set the overlays we want to use.
  this.constructors.read = ReadOverlayManager;
  this.constructors.compose = ComposeOverlayManager;
}

Controller.prototype = new ControllerBase();
Controller.prototype.constructor = Controller;

/**
 * Start the controller. If both skipReadOverlays and skipComposeOverlays are true, ControllerBase will handle watching
 * for moved and removed overlays.
 */
Controller.prototype.start = function () {
  ControllerBase.prototype.start.call(this);

  // Grab subject items and annotate them.
  this.observer.observe({added: true, modified: true, subtree: true}, '.y6>span:first-child', function (item) {
    if (PackageWrapper.isWrappedSubject(item.innerText)) {
      item.innerText = PackageWrapper.unwrapSubject(item.innerText);
      var lockButton = document.createElement('div');
      lockButton.className = 'messageGuardLockDiv';
      lockButton.innerText = 'Encrypted';
      jQuery(item).before(lockButton);
    }
  }.bind(this));

  // Run after everything is finished.
  var tutorial = new Tutorial(this.overlayManagers);
  window.setTimeout(function () {
    tutorial.initialRun();
  }, 10);
};

/**
 * Scan the given node for read packages and overlay them.
 * @param {!Element} node Node to scan.
 */
Controller.prototype.scanForReadPackage = function (node) {
  var encryptedPackage = node.querySelector('div[name="package"]');
  if (!encryptedPackage) {
    return;
  }

  this.addOverlayManager(node, this.constructors.read);
};

module.exports = Controller;
